package ch.hevs.isi.core;

import ch.hevs.isi.db.TimeManager;

import java.sql.Timestamp;

public interface DataPointListener {


    /** interface thus without programming body for NewValue
     *
     * @param db hand over DataPoint
     */

    void onNewValue(DataPoint db);
}