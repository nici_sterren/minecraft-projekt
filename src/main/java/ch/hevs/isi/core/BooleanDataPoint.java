package ch.hevs.isi.core;

import ch.hevs.isi.db.DatabaseConnector;

    /** class extends DataPoint */
public class BooleanDataPoint extends DataPoint {

    /** Attribute */
    private boolean value;



        /** Constructor (Superclass)
         *
         * @param label String for Data
         * @param isOutput boolean for Data
         */

    public BooleanDataPoint(String label, boolean isOutput) {

        super(label, isOutput);
    }

        /** return value in string
         * @return value of object
         */

        @Override
    public String toString() {

        return String.valueOf(value);
    }


        /** method get value
         *
         * @return boolean value
         */
    public boolean getValue(){
            return value;
    }

        /** method set value and push in connectors
         *
         * @param value is a boolean to set
         */
    public void setValue(boolean value){
        this.value=value;
        pushToConnectors();
    }
}
