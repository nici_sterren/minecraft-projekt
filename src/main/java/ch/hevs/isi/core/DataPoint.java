package ch.hevs.isi.core;

import ch.hevs.isi.db.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;

import javax.xml.crypto.Data;
import java.util.HashMap;

public abstract class DataPoint {

    /** Attributes */
    private String label;
    private boolean isOutput;

    /** create HashMap */
    private static HashMap<String,DataPoint> dataPointMap = new HashMap<>();


    /** Constructor Datapoint and push Datapoint into the HashMap
     *
     * @param label String which is selected
     * @param isOutput boolean which is selected
     */
    protected DataPoint (String label, boolean isOutput){
        this.label=label;
        this.isOutput=isOutput;

        dataPointMap.put(label,this);
    }

    /** method find out datapoint with label
     *
     * @param label String to find out
     * @return DataPoint
     */
    public static DataPoint getDataPointFromLabel (String label) {

        return dataPointMap.get(label);
    }

    /** method get Label
     * *
     * @return the label
     */
    public String getLabel() {
        return this.label;
    }

    /** method to set value boolean
     *
     * @param value is a boolean to set
     */
    public void setValue(boolean value) {}

    /** method to set value float
     *
     * @param value is a float to set
     */
    public void setValue(float value) {}

    /** method to get Value(abstract)
     *
     * @return the type in override classes/methods
     */
    public abstract String toString();

    /** method to push in Connector by DatabaseConnector/WebConnector/FieldConnector
     *
     */
    protected void pushToConnectors() {
        DatabaseConnector.getInstance().onNewValue(this);
        WebConnector.getInstance().onNewValue(this);

        if (isOutput)
            FieldConnector.getInstance().onNewValue(this);

    }

    /** test class DataPoint
     *
     * @param args
     */
    public static void main(String[] args) {
        /** Testing updating of inputs */
            /** Instance  */
        BooleanDataPoint test = new BooleanDataPoint("test",false);
        FloatDataPoint test2 = new FloatDataPoint("test2",true);
        test.setValue(true);
        test2.setValue(17f);

            /** display */
        System.out.println("test.value = " + test.getValue());;


        /** Testing updating of outputs */
            /**  Instance without declaration */
        new FloatDataPoint("Test1", false);
            /** calling the method */
        FloatDataPoint fdp = (FloatDataPoint) DataPoint.getDataPointFromLabel("Test1");
            /** set value, if fdp is found */
        if (fdp != null)
            fdp.setValue(2f);

    }
}
