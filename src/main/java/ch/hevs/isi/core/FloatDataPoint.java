package ch.hevs.isi.core;

import ch.hevs.isi.db.DatabaseConnector;

/** class extends DataPoint */
public class FloatDataPoint extends DataPoint{

    /** Attribute */
    private float value;


    /**Constructor (Superclass)
     *
     * @param label String for Data
     * @param isOutput boolean for Data
     */
    public FloatDataPoint (String label, boolean isOutput){

        super(label, isOutput);
    }


    /** return value in string
     *
     * @return value of object
     */
    @Override
    public String toString() {

        return String.valueOf(value);
    }

    /** method get value
     *
     * @return float value
     */
    public float getValue(){
        return value;
    }


    /** method set value and push in connectors
     *
     * @param value is a float to set
     */
    public void setValue(float value){
        this.value=value;
        pushToConnectors();
    }

}
