package ch.hevs.isi;

import ch.hevs.isi.db.DatabaseConnector;
import ch.hevs.isi.field.*;
import ch.hevs.isi.utils.Utility;

import java.io.BufferedReader;
import java.io.IOException;

public class MinecraftController {

    public static boolean ERASE_PREVIOUS_DATA_INB_DB        = false;

    public static void usage() {
        System.out.println();
        System.out.println("You're reading this message because no parameter (or not the needed ones) has been passed to the application.");
        System.out.println();
        System.out.println("In development mode, just add to your running configuration the needed parameters (see usage below).");
        System.out.println("In running mode, the application's usage is the following:");
        System.out.println("java MinecraftController <InfluxDB Server> <DB Name> <DB Measurement> <DB Username> <ModbusTCP Server> <ModbusTCP port> [-modbus4j] [-keepAlive]");
        System.out.println("where:");
        System.out.println("- <InfluxDB Server>:  The complete URL of the InfluxDB server, including the protocol (http or https)...");
        System.out.println("                      Example: https://influx.sdi.hevs.ch");
        System.out.println("- <DB Name>:          The name of the Influx DB to use. For this project, this name is the name of the group you've been affected to. (SInXX)");
        System.out.println("- <DB Username:       The user's name to use to access the DB. It's also your group's name. (SInXX)");
        System.out.println("- <ModbusTCP Server>: The IP address of the Minecraft ModbusTCP server (default value: localhost)");
        System.out.println("- <ModbusTCP port>:   The port number of the Minecraft ModbusTCP server (default value: 1502)");
        System.out.println("- [-eraseDB]:         Optional parameter! If set, the application will erase the previous data in InfluxDB...");
        System.out.println();
        System.exit(1);
    }

    @SuppressWarnings("all")
    public static void main(String[] args) {

        // ------------------------------------- DO NOT CHANGE THE FOLLOWING LINES -------------------------------------
        String dbProtocol       = "http";
        String dbHostName       = "localhost";
        String dbName           = "labo";
        String dbUserName       = "root";
        String dbPassword       = "root";

        String modbusTcpHost    = "localhost";
        int modbusTcpPort       = 1502;

        // Check the number of arguments and show usage message if the number does not match.
        String[] parameters = null;

        // If there is only one number given as parameter, construct the parameters according the group number.
        if (args.length >= 5) {
            parameters = args;

            // Decode parameters for influxDB
            String[] dbParams = parameters[0].split("://");
            if (dbParams.length != 2) {
                usage();
            }

            dbProtocol    = dbParams[0];
            dbHostName    = dbParams[1];
            dbName        = parameters[1];
            dbUserName    = parameters[2];
            dbPassword    = Utility.md5sum(dbUserName);

            // Decode parameters for Modbus TCP
            modbusTcpHost = parameters[3];
            modbusTcpPort = Integer.parseInt(parameters[4]);

            for (int i = 5; i < args.length; i++) {
                if (parameters[i].compareToIgnoreCase("-erasedb") == 0) {
                    ERASE_PREVIOUS_DATA_INB_DB = true;
                }
            }
        } else {
            usage();
        }

        // ------------------------------------ /DO NOT CHANGE THE FOLLOWING LINES -------------------------------------


        // Start coding here ...
       /** create an object of Database with our token */
        DatabaseConnector.getInstance().initialize(dbProtocol, dbHostName, dbName, "1edbvturTx9CpybIQ0bYRdaanRlL2FE9C45rB7yU2QmZN4L7XDlBGO-hSc1BQzEY8PIqFVAKBcvn8n_gaVLmxA==");

        /** get Modbus Registers into from CSV file */
        BufferedReader br = Utility.fileParser(null, "ModbusMap.csv");
        if (br != null) {
            try {
                /** read one line of the csv file */
                String line = br.readLine();
                while (line != null) {
                    if (!line.startsWith("Label")) {
                        /** split the line and push it into an array */
                        String val[] = line.split(";");

                        /** check if the type is a float and create a new FloatRegister object with the corresponding parameters */
                        if (val[1].equals("F")) {
                            FloatRegister f1 = new FloatRegister(val[0],val[3].equals("Y") ,Integer.parseInt(val[4]));
                        }

                        /** check if the type is a boolean and create a new Booleanregister object with the corresponding parameters */
                        else {
                            BooleanRegister b1 = new BooleanRegister(val[0],val[3].equals("Y") ,Integer.parseInt(val[4]));
                        }
                    }
                    /** read the next line of the csv file */
                    line = br.readLine();
                }
            } catch (IOException e) {
            }
        }

        /** connect the master */
        ModbusAccessor.getInstance().connect("localhost", 1502);
        FieldConnector.getInstance().poll(2000);
//        FieldConnector.getInstance().poll(3000);

//        FloatRegister f1 = new FloatRegister("GRID_U_FLOAT",false,89);

    }
}
