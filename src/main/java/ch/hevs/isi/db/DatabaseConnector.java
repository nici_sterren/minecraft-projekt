package ch.hevs.isi.db;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.field.FieldConnector;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Timestamp;

public class DatabaseConnector implements DataPointListener {

    /** private Attribute for Database */
    private String protocol_ = null;
    private String hostname_;
    private String bucket_;
    private String token_;
    private String project_token="1edbvturTx9CpybIQ0bYRdaanRlL2FE9C45rB7yU2QmZN4L7XDlBGO-hSc1BQzEY8PIqFVAKBcvn8n_gaVLmxA==";
    private static DatabaseConnector instance = null;

    /** public Attribute for Time management */
    public TimeManager _timeManager = new TimeManager(3);
    public long _timestamp=0;

    /** private Constructor */
    private DatabaseConnector(){};

    /** The static method getInstance() returns a reference to the singleton.
     * It creates the single DatabaseConnector object if it does not exist
     * @return instance Object of DatabaseConnector
     */
    public static DatabaseConnector getInstance() {

        if (instance == null) {

            instance = new DatabaseConnector();
        }

        return instance;
    }


    /** Method to initialize an instance of DatabaseConnector
     *
     * @param protocol String to set the protocol
     * @param hostname String to set the hostname
     * @param bucket String to set the bucket in our case SIn06
     * @param token String to set the token
     */
    public void initialize(String protocol, String hostname, String bucket, String token ) {
        protocol_ = protocol;
        hostname_ = hostname;
        bucket_ = bucket;
        token_ = token;

    }

    /** Method to push a new value to Database (with included timeManager)
     *
     * @param db hand over DataPoint
     */
    @Override
    public void onNewValue(DataPoint db) {
        /** check clock float flag and set the timestamp */
        if (db.getLabel().equals("CLOCK_FLOAT")) {
            _timeManager.setTimestamp(db.toString());
            _timestamp = _timeManager.getNanosForDB();
        }
        /** push to Database if the timestamp isn't equal to zero */
        if (_timestamp != 0) {
            pushToDatabase(db.getLabel(), db.toString());
        }
    }


    /** display which is pushed to database with label + value
     * method for connect HTTP with Java
     *
     *
     * @param label string (displays it)
     * @param value string (displays it)
     */
    private void pushToDatabase (String label, String value){

        /** test the Database with a println */
        System.out.println("Database: " + label + " " + value);

        try {

            /** create URL object */
            URL url1 = new URL(protocol_ + "://" + hostname_ + "/api/v2/write?org=" + bucket_ + "&bucket=" + bucket_);
            /** call the URL object openConnection() to retrieve an HttpURLConnection object */
            HttpURLConnection connection = (HttpURLConnection) url1.openConnection();

            /** configure the HttpURLConnection */
            connection.setRequestProperty ("Authorization", "Token " + project_token);
            connection.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);

            /** fetch an OutputStreamWriter object for the HttpURLConnection */
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());

            /** write body of the post request into the OutputStreamWriter */
            writer.write(label + " value=" + value+" "+ _timestamp);
            writer.flush();

           /** wait for responseCode */
            int responseCode=connection.getResponseCode();

            /** if the responseCode isn't 204 send an error to the console */
            if(responseCode!=204){
                System.out.println(label + " value=" + value+" "+ _timestamp);
                System.out.println("can't push it to Database "+ responseCode);
            }

            writer.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

}



