package ch.hevs.isi.web;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.field.FieldConnector;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import javax.xml.bind.annotation.XmlInlineBinaryData;
import java.net.InetSocketAddress;
import java.util.LinkedList;
import java.util.List;

public class WebConnector implements DataPointListener {

        /** private Attribute */
    private static WebConnector instance = null;

    /** list for WebSocket */
    private List<WebSocket> webSocketList = new LinkedList<>();


    /**private constructor
     *
     */
    private WebConnector(){
        /** create new WebSocketServer with PortNbr. 8888 */
        WebSocketServer wss = new WebSocketServer(new InetSocketAddress(8888)) {

            /** send a message to the client and add to the list
             *
             * @param webSocket Object WebSocket
             * @param clientHandshake
             */
            @Override
            public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
                webSocket.send("Welcome to Minecraft");
                webSocketList.add(webSocket);
            }

            /** methode to remove a WebSocket object from the list
             *
             * @param webSocket Object WebSocket
             * @param i
             * @param s
             * @param b
             */
            @Override
            public void onClose(WebSocket webSocket, int i, String s, boolean b) {
                webSocketList.remove(webSocket);

            }

            /** methode to set the label and value to the correspondent DataPoint with method getDataPointFromLabel
             *
             * @param webSocket Object WebSocket
             * @param s Message
             */
            @Override
            public void onMessage(WebSocket webSocket, String s) {
            if(s!= null){
                String[] message = s.split("=");
                if(message.length>=2){
                    String label = message[0];
                    String value = message[1];
                    /** check if the value is a boolean and write in BooleanDataPoint*/
                    if(value.equals("true") || value.equals("false")){
                        System.out.println(label + value);
                        BooleanDataPoint bdp = (BooleanDataPoint) DataPoint.getDataPointFromLabel(label);
                        bdp.setValue(Boolean.parseBoolean(value));
                    }
                    /** write in FloatDataPoint*/
                    else{
                        System.out.println(label + value);
                        FloatDataPoint fdp = (FloatDataPoint) DataPoint.getDataPointFromLabel(label);
                        fdp.setValue(Float.parseFloat(value));

                    }
                }
            }
            }

            /** methode to remove the object (error)
             *
             * @param webSocket Object WebSocket
             * @param e
             */
            @Override
            public void onError(WebSocket webSocket, Exception e) {
                webSocketList.remove(webSocket);
            }

            /** Methode to show, we are connected
             *
             */
            @Override
            public void onStart() {
                System.out.println("Connect");

            }
        };
        /** start the WebConnector */
        wss.start();
    }



    /** The static method getInstance() returns a reference to the singleton.
     * It creates the single WebConnector object if it does not exist
     * @return instance Object of WebConnector
     */
    public static WebConnector getInstance() {

        if (instance == null) {

            instance = new WebConnector();
        }

        return instance;
    }



    /** Display which is pushed to web with label + value
     *
     * @param label string (displays it)
     * @param value string (displays it)
     */
    private void pushToWeb (String label, String value){
        System.out.println("Web: " + label + " " + value);
        // For each webSocket -> send the new value
        for (WebSocket ws: webSocketList) {
            ws.send(label + "=" + value);
        }
    }




    /** method (override) push it to Web
     *
     * @param db hand over DataPoint
     */
    @Override
    public void onNewValue(DataPoint db) {
        pushToWeb(db.getLabel(),db.toString());

    }

    public static void main(String[] args) {
        WebConnector.getInstance();
    }
}

