package ch.hevs.isi.field;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.FloatDataPoint;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusTransportException;

import java.util.HashMap;

public class FloatRegister extends ModbusRegister {

    /** private Attribute */
    private FloatDataPoint dataPoint;

    /** constructor FloatRegister
     *
     * @param label String for data
     * @param isOutPut boolean for data
     * @param address int for data
     */
    public FloatRegister(String label, boolean isOutPut, int address){

        this.dataPoint = new FloatDataPoint(label,isOutPut);
        setRegister(dataPoint, address);
    }

    /** method for read the new float value */
    public void read(){

        try {

           dataPoint.setValue(ModbusAccessor.getInstance().readFloat(address));
           System.out.println(ModbusAccessor.getInstance().readFloat(address));

        } catch (ModbusTransportException e) {
            System.out.println("ModbusTransportException: " + e.getMessage());
        } catch (ErrorResponseException e) {
            System.out.println("ErrorResponseException: " + e.getMessage());
        }

    }

    /** method for write the new float value */
    public void write(){

        try {

            ModbusAccessor.getInstance().writeFloat(address, dataPoint.getValue());

        } catch (ModbusTransportException e) {
            throw new RuntimeException(e);
        } catch (ErrorResponseException e) {
            throw new RuntimeException(e);
        }

    }


}
