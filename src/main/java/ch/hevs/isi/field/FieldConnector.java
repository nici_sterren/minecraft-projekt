package ch.hevs.isi.field;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.utils.Utility;
import com.sun.xml.internal.bind.v2.model.annotation.FieldLocatable;

import java.util.Timer;
import java.util.TimerTask;

public class FieldConnector implements DataPointListener {


    /** private Attribute */
    private static FieldConnector instance = null;


    /** private constructor */
    private FieldConnector(){};

    /** The static method getInstance() returns a reference to the singleton.
     * It creates the FieldConnector object if it does not exist
     * @return instance Object of FieldConnector
     */
    public static FieldConnector getInstance() {

        if (instance == null) {

            instance = new FieldConnector();
        }

        return instance;
    }

    /** Display which is pushed to field with label + value for testing the FieldConnector
     *
     * @param label string (displays it)
     * @param value string (displays it)
     */
    private void pushToField (String label, String value){

        System.out.println("Field: " + label + " " + value);
    }



    /** method (override) push it to Field
     *
     * @param db hand over DataPoint
     */
    @Override
    public void onNewValue(DataPoint db) {
        //pushToField(db.getLabel(),db.toString());

        ModbusRegister mr = ModbusRegister.getRegisterFromDataPoint(db);
        mr.write();

       //BooleanRegister.getRegisterFromDataPoint(db).write();

    }

    /** public method for creat a new timer and for run through the map
     *
     * @param period int for set the period
     */
    public void poll(int period){

        Timer t1 = new Timer();
        t1.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                ModbusRegister.poll();
                SmartControl.poll();
            }
        },2000,period);

    }

    public   static void main(String[] args) {

        /** two new BooleanRegister for test the poll() method */
        BooleanRegister b1  = new BooleanRegister("SOLAR_CONNECT_ST",false,609);
        BooleanRegister b2  = new BooleanRegister("WIND_CONNECT_ST",false,613);
        BooleanRegister solarSwitch = new BooleanRegister("REMOTE_SOLAR_SW", true, 401);

        /** connect the master */
        ModbusAccessor.getInstance().connect("localhost", 1502);
        FieldConnector.getInstance().poll(2000);


        /** change the solar with the remote control */
        boolean solarOn = false;
        while (true) {
            BooleanDataPoint bdp = (BooleanDataPoint) DataPoint.getDataPointFromLabel("REMOTE_SOLAR_SW");
            solarOn = solarOn ? false : true;
            bdp.setValue(solarOn);

            /** wait for 4000ms */
            Utility.waitSomeTime(4000);
        }
    }
}
