package ch.hevs.isi.field;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusTransportException;

import java.util.HashMap;


public class BooleanRegister extends ModbusRegister {

    /** private attribute */
    private BooleanDataPoint dataPoint;


    /** constructor BooleanRegister
     *
     * @param label String for data
     * @param isOutPut boolean for data
     * @param address int address for data
     */
    public BooleanRegister(String label, boolean isOutPut, int address){

        this.dataPoint = new BooleanDataPoint(label,isOutPut);
        setRegister(dataPoint, address);

    }

    /** method for read the new boolean value */
    public void read() {

        try {

            dataPoint.setValue(ModbusAccessor.getInstance().readBoolean(address));
            System.out.println(ModbusAccessor.getInstance().readBoolean(address));

        } catch (ModbusTransportException e) {
            System.out.println("ModbusTransportException: " + e.getMessage());
        } catch (ErrorResponseException e) {
            System.out.println("ErrorResponseException: " + e.getMessage());
        }

    }

    /** method for write the new boolean value */
    public void write(){

        try {

            ModbusAccessor.getInstance().writeBoolean(address, dataPoint.getValue());

        } catch (ModbusTransportException e) {
            throw new RuntimeException(e);
        } catch (ErrorResponseException e) {
            throw new RuntimeException(e);
        }

    }

}
