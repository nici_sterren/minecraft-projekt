package ch.hevs.isi.field;

import ch.hevs.isi.utils.Utility;
import com.serotonin.modbus4j.ModbusFactory;
import com.serotonin.modbus4j.ModbusMaster;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import com.serotonin.modbus4j.ip.IpParameters;
import com.serotonin.modbus4j.locator.BaseLocator;

public class ModbusAccessor {

    /** Attribute */
    public static boolean keepAlive = true;

    /** private attributes */
    private ModbusMaster master = null;
    private static ModbusAccessor x_c = null;


    /** method to connect to the selected server (create TCP-Master)
     *
     * @param IpAddress string to connect
     * @param port the port number of the selected server
     */
    public void connect(String IpAddress, int port){
    ModbusFactory MF = new ModbusFactory();
    IpParameters IpP = new IpParameters();
    IpP.setHost(IpAddress);
    IpP.setPort(port);
    this.master=MF.createTcpMaster(IpP, keepAlive);
    try{
        master.init();
    } catch (ModbusInitException e) {
        e.printStackTrace();
    }

}

    /** method read boolean of selected address
     *
     * @param adress which is selected
     * @return state (boolean) from master which is selected
     * @throws ModbusTransportException
     * @throws ErrorResponseException
     */

    public boolean readBoolean(int adress) throws ModbusTransportException, ErrorResponseException {
    return master.getValue(BaseLocator.coilStatus(1, adress));
   }

    /** method read float of selected address
     *
     * @param adress which is selected
     * @return value from master which is selected
     * @throws ModbusTransportException
     * @throws ErrorResponseException
     */
    public float readFloat(int adress) throws ModbusTransportException, ErrorResponseException {
        float floatValue = (float) master.getValue(BaseLocator.holdingRegister(1, adress, DataType.FOUR_BYTE_FLOAT));
        return floatValue;

    }

    /** method set state (boolean) of selected address
     *
     * @param adress which is selected
     * @param value state to write in address
     * @throws ModbusTransportException
     * @throws ErrorResponseException
     */
    public void writeBoolean(int adress, boolean value) throws ModbusTransportException, ErrorResponseException {
        master.setValue(BaseLocator.coilStatus(1, adress),value);
    }

    /** method set value (float) of selected address
     * @param adress which is selected
     * @param value value to write in address
     * @throws ModbusTransportException
     * @throws ErrorResponseException
     */
    public void writeFloat(int adress, float value) throws ModbusTransportException, ErrorResponseException {
        master.setValue(BaseLocator.holdingRegister(1, adress, DataType.FOUR_BYTE_FLOAT),value);
    }

    /** private constructor */
    private ModbusAccessor(){};

    /** The static method getInstance() returns a reference to the singleton.
     *  It creates the single ModbusAccessor object if it does not exist
     * @return instance Object of ModbusAccessor
     */
    public static ModbusAccessor getInstance(){
        if(x_c==null){
            x_c = new ModbusAccessor();
        }
        return x_c;
    }

    /** test class ModbusAccessor
     */
    public static void main(String[] args) {
        /** connect to server */
        ModbusAccessor test = ModbusAccessor.getInstance();
        test.connect("LocalHost", 1502);

        while (true) {
            try {
                boolean solar = test.readBoolean(609);
                System.out.println("Solar is ON ?" + (solar ? "YES":"NO"));
            } catch (ModbusTransportException e) {
                //throw new RuntimeException(e);
            } catch (ErrorResponseException e) {
                //throw new RuntimeException(e);
            }

            Utility.waitSomeTime(2000);
        }
    }
}
