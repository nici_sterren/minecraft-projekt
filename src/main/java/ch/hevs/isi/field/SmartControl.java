package ch.hevs.isi.field;

import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;
import org.apache.commons.lang3.ObjectUtils;

import java.util.Timer;
import java.util.TimerTask;

public class SmartControl {

    public SmartControl(){
                /** get the FloatDataPoint object for the label BATT_CHRG_FLOAT */
                FloatDataPoint batt_chrg = (FloatDataPoint) DataPoint.getDataPointFromLabel("BATT_CHRG_FLOAT");

                /** get the FloatDataPoint objects to set the values */
                FloatDataPoint remote_coal_sp = (FloatDataPoint) DataPoint.getDataPointFromLabel("REMOTE_COAL_SP");
                FloatDataPoint remote_factory_sp = (FloatDataPoint) DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP");

                        /** check the value of batt_chrg (batt_chrg != null) and regulate the values with the setpoints */
                        if( batt_chrg != null && batt_chrg.getValue()<=0.2f){
                            remote_coal_sp.setValue(1f);
                            remote_factory_sp.setValue(0f);
                        }

                        if(batt_chrg != null && batt_chrg.getValue()>0.2f && batt_chrg.getValue()<0.4f ){
                            remote_coal_sp.setValue(1f);
                            remote_factory_sp.setValue(0f);
                        }

                       if(batt_chrg != null && batt_chrg.getValue()>=0.4f && batt_chrg.getValue()<0.6f ){
                            remote_coal_sp.setValue(0f);
                            remote_factory_sp.setValue(0f);
                       }


                        if(batt_chrg != null && batt_chrg.getValue()>=0.6f && batt_chrg.getValue()<=0.9f){
                             remote_coal_sp.setValue(0f);
                             remote_factory_sp.setValue(0.8f);
                         }

                        if(batt_chrg != null && batt_chrg.getValue()>=0.9f ){
                            remote_coal_sp.setValue(0f);
                            remote_factory_sp.setValue(1f);
                       }

                    }

    /** make a new SmartControl object */
    public static void poll() {

       new SmartControl();

    }


}


