package ch.hevs.isi.field;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPoint;

import java.util.HashMap;

public abstract class ModbusRegister {

    /** protected attribute */
    protected int address;

    /** new HashMap with objects of DataPoint */
    public final static HashMap<DataPoint, ModbusRegister> map = new HashMap<>();

    /** protected method for set the DataPoint object in the map
     *
     * @param dp DataPoint for set the object
     * @param address int for set the address
     */
    protected void setRegister(DataPoint dp, int address) {
        this.address = address;
        map.put(dp, this);
    }

    /**public method for get the register from DataPoint
     *
     * @param dp DataPoint for set the object
     * @return the register of the DataPoint object
     */
    public static ModbusRegister getRegisterFromDataPoint(DataPoint dp) {
        return map.get(dp);
    }

    /** public abstract prototype of the method read() */
    public abstract void read();
    /** public abstract prototype of the method write() */
    public abstract void write();

    /** method for run through the map and activate the method read() */
    public static void poll() {

        for (ModbusRegister fr : map.values()) {

            fr.read();

        }
    }
}
