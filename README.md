# Minecraft Base Project




IntelliJ base project related to Minecraft Lab of 221_SIn & 231_SIn course




The aim of this project is to establish the connection between Minecraft and our programme in Java. To do this, we are to display various data in real time on the InfluxDB website. Likewise, our values will be graphically displayed in real time on the website Grafana.




-Core Component

    The core component has the same properties as the field process. This component has the same parameters.




    A connection is to be established with the database, web and Smart Control.




    All changes are written to a DataPoint. Both inputs and outputs. Each DataPoint has a value, a label and says whether it is an output.

    When a change is made in the field input, the <<DataPoint>> forwards the changes to the <<DatabaseConnector>>, <<WebConnector>> and <<FieldConnector>>.




-Modbus Component

    Modbus can be used to communicate between the Java programme and ElectricalAge.

    Modbus is a binary protocol (TCP). The Java programme is the client and the Minecraft is the server.

    Operations on registers are performed.




-Field Component

    The <<DataPoints>> and the field are connected to the registers.

    No value is stored in the registers.

    When the read() method is called on a <<FloatRegister>> / <<Booleanregister>>, the value is updated in the corresponding <<DataPoint>>.

    When the write() method is called on a <<FloatRegister>> / <<Booleanregister>>, the current value is written to the corresponding <<DataPoint>> on the Modbus.

    A PollTask class organises the periodic polling of all registers.




-Database Component

    The aim is to store the different values of the field in a database. In our case, the database is InfluxDB (https://influx.sdi.hevs.ch/signin). The values are displayed graphically in real time on Grafana (https://grafana.sdi.hevs.ch/?orgId=28).

    In this world, a day lasts 10 minutes. The complete simulation of the world takes 25 minutes.




-Web Component

    The aim of this component is to display and change values from different websites. This happens in real time.

    The human being can thus operate the programme.




-Smart Control

    The aim of this component is to control the values intelligently. To do this, we compare consumption and production. And with this comparison we can set different values.

    We have to make sure that the batteries never reach their limits.





The javadoc can be found under the following link <https://gitlab.com/nici_sterren/minecraft-projekt/-/tree/master/javadoc/hei>





To start the game, you may need the jar file. It is located at <https://gitlab.com/nici_sterren/minecraft-projekt/-/tree/master/out/artifacts/Minecraft_jar>

The game can be started with the help of cmd.




To start the programme, we can start with the following arguments:

<db_url> <db_org> <db_bucket> <db_tocken> <modbus_host> <modbus_port>

To start the programme, you can enter the following line:

java -jar MinecraftController.jar https://influx.sdi.hevs.ch SIn06 SIn06 1edbvturTx9CpybIQ0bYRdaanRlL2FE9C45rB7yU2QmZN4L7XDlBGO-hSc1BQzEY8PIqFVAKBcvn8n_gaVLmxA== 127.0.0.1 1502